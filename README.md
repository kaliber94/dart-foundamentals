**Dart Apprentice: Fundamentals**

 Nov 2 2022 , Dart 2.18, Flutter, VS Code 1.71
 
Dart Apprentice: Fundamentals is the first of a two-book series that will teach you all the basic concepts you need to master this powerful and versatile language.
