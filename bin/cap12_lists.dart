////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 12: Lists
////////////////////////////////////////////////////////////////////////////////////////////////////////

void main() {
  // Declare
  var desserts = ['cookies', 'cupcakes', 'donuts', 'pie'];
  List<String> snacks = [];
  var snakss = <String>[];

  // Accessing element
  final index = desserts.indexOf('cupcakes');
  final element = desserts[index];
  print('The value at index $index is $element');

  // Handle values
  desserts[1] = 'cake';
  desserts.add('brownies');
  desserts.insert(1, 'ice cream');
  desserts.remove('pie');
  desserts.removeAt(2);
  final first = desserts.first;
  final latest = desserts[desserts.length - 1];
  print(desserts);
  print('firs is $first and latest is $latest');

  // Sorting
  final integers = [11, 33, 2, 342, 10];
  integers.sort();
  print(integers);

  // Mutable and immutable
  final desserts2 = ['cookies', 'donuts'];
  // desserts2 = ['john']; not allowed
  desserts2.removeAt(0); // allowed

  const desserts3 = ['cookies', 'donuts'];
  // desserts3 = ['john']; // NOT allowed
  // desserts3.removeAt(0); // NOT allowed

  // I u need an immutable list but won't know the element values untile runtime, use List.unmodifiable constructor
  final modifiableList = [DateTime.now(), DateTime.now()];
  final unmodifiableList = List.unmodifiable(modifiableList);

  // List properties
  const drinks = ['water', 'milk', 'juice', 'soda', 'gin tonic'];
  drinks.first;
  drinks.last;
  drinks.isEmpty;
  drinks.isNotEmpty;
  drinks.length;

  for (int i = 0; i < drinks.length; i++) {
    final item = drinks[i];
    print('I like $item');
  }

  for (final item in drinks) {
    print('I also like $item');
  }

  drinks.forEach((element) {
    print('Happy $element');
  });

  // Spread Operator
  const pastries = ['cookies', 'croissants'];
  const candy = ['Junior Mints', 'M&M', 'Twizzlers'];

/*
desserts.addAll(pastries);
desserts.addAll(candy);

the '...' operator takes the elements and adds them
*/
  const finalDesserts = ['donuts', ...pastries, ...candy];
  print(finalDesserts);

  const peanutAllergy = true;

  const sensitiveCandy = [
    'Junior mints',
    'twizzlers',
    if (!peanutAllergy) 'reeses',
  ];
  print(sensitiveCandy);

  const worldDesert = ['gobi', 'sahara', 'artic'];
  var uppercasedWorldDesert = [
    'ARABIAN',
    for (var item in worldDesert) item.toUpperCase()
  ];
  print(uppercasedWorldDesert);


  // Nullable 
  List<int>? nullableList = [2, 3, 4 , 7];
  nullableList = null;

  List<int?> nullableElementList = [2, 3, 4 , 7];
  // nullableElementList = null; CAN'T DO
  nullableElementList[0] = null;
  print(nullableElementList);

  List<String>? coffees;
  final hotDrinks = ['milk tea', ...?coffees];
  print(hotDrinks);
}

class Desserts {
  // Initialize final variable desserts with a immutable list
  Desserts([this.desserts = const ['cookies']]);
  final List<String> desserts;
}
