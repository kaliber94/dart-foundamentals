////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 8/9: Classes - Constructor
////////////////////////////////////////////////////////////////////////////////////////////////////////

class Password {
  // private params
  String _plainText = '123456';

  String getPlainText() {
    return _plainText;
  }

  // getter
  String get plainText => _plainText;

  // calculated get property
  String get obsufscated {
    final lenght = _plainText.length;
    return '*' * lenght;
  }

  // setter
  set plainText(String newText) => _plainText = newText;

  // calculated set property
  set newPassword(String newText) {
    if (newText.length < 6) {
      print('Password required 6 characters');
    } else {
      _plainText = newText;
    }
  }
}

class UserWithoutConstructor {
  int id = 0;
  String name = '';
}

class User {
  // Long Constructor
  /*
  User(int id, String name) {
    this.id = id;
    this.name = name;
  }
  */

  // Short Constructor
  /*
  User(this.id, this.name);
  */

  // Short Constructor with default value
  User({this.id = 0, this.name = 'anonymous'});

  int id;
  String name;

  // Named Constructor
  /*
  User.anonymous() {
    id = 0;
    name = 'anonymous';
  }*/

  // Forwarding Constructor
  /*
  User.anonymous() : this(0, 'anonymous');
  */

  // Forwarding Constructor with default value
  User.anonymous() : this();

  // Methods
  @override
  String toString() {
    return 'User - Name: $name, ID: $id';
  }

  String toJSON() {
    return '{"id":$id, "name":$name}';
  }
}

// Recap constructor
class NewUser {
  // Unnamed constructor
  NewUser({int id = 0, String name = 'anonymous'})
      : _id = id,
        _name = name {
    print('User created with name $_name');
  }

  // Named Constructor
  NewUser.anonymous() : this();

  // Private properties
  int _id;
  String _name;
  /*
  NB: private propery are private in Dart file, not in Dart class.
  So, in this file u can read and modify these properties
  */

  // Factory Constructor
  factory NewUser.ray() {
    return NewUser(id: 42, name: 'Ray');
  }

  factory NewUser.fromJson(Map<String, Object> json) {
    final userId = json['id'] as int;
    final userName = json['name'] as String;
    return NewUser(id: userId, name: userName);
  }

  // Factory-named Constructor
  NewUser.json(Map<String, Object> json)
      : _id = json['id'] as int,
        _name = json['name'] as String;

  @override
  String toString() {
    return 'NewUser - Name: $_name, ID: $_id';
  }

  String toJSON() {
    return '{"id":$_id, "name":$_name}';
  }
}

// Costant Constructor
class Dog {
  final String _name;
  final int _id;

// Const class is immutable
  const Dog({int id = 0, String name = 'Ciro'})
      : _id = id,
        _name = name;

  const Dog.anonymous() : this();
}

void main() {
  final secondUser = UserWithoutConstructor()
    ..name = 'Gennaro'
    ..id = 4;

  final user = User(id: 10, name: 'Joe');
  user.name = 'Joe';
  user.id = 10;

  print(user.toString());
  print(user.toJSON());

  final anonymousUser = User.anonymous();
  print(anonymousUser);

  final password = Password();
  print(password.getPlainText());
  print(password.obsufscated);

  password.plainText = "abcdefg";
  print(password.plainText);
  password.newPassword = "12345";
  password.newPassword = "12345a";
  print(password.plainText);

  final vicki = NewUser(id: 24, name: 'Vicki');
  vicki._name = 'hei';
  print(vicki);

  const dog = Dog(id: 0, name: 'Pluto');

  final map = {'id': 10, 'name': 'Ciro Esposito'};
  final ciro = NewUser.fromJson(map);
  print(ciro);

  final json = {'id': 11, 'name': 'Gennaro'};
  final gennaro = NewUser.json(json);
  print(gennaro);
}