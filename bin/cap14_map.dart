import 'dart:convert';

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 14: Map
////////////////////////////////////////////////////////////////////////////////////////////////////////

void main() {
  final Map<String, int> emptyMap = {}; // method1
  final emptyMap2 = <String, int>{}; // method2

  final emptySomething = {}; // dart see it as map of <dynamic, dynamic>
  final mySet = <String>{}; // instead in this way dart recognize as set

  final inventory = {
    'cakes': 20,
    'pies': 14,
    'donuts': 37,
    'cookies': 141,
  };

  final digitToWord = {
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
  };

  print(inventory);
  print(digitToWord);

// Operation on a Map
  final numberOfCakes = inventory['cakes'];
  print(numberOfCakes?.isEven);
  inventory['cakes'] = 1;
  inventory.remove('cookies');
  print(inventory);

// Accessing properties
  inventory.isEmpty;
  inventory.isNotEmpty;
  inventory.length;

  final exists = inventory.containsKey('pies');
  inventory.containsValue(2);

  for (var key in inventory.keys) {
    print(inventory[key]);
  }

  for (final entry in inventory.entries) {
    // iterate entries
    print('${entry.key} -> ${entry.value}');
  }

  final userObject = User(
    id: 1234,
    name: 'John',
    emails: ['john@example.com', 'ciro@example.com'],
  );

  final userMap = userObject.toJson();
  print(userMap);

  // Converting a Map to a JSON String
  final userString = jsonEncode(userMap);
  print(userString);

  final ciroJsonString =
      '{"id":12,"name":"Ciro Esposito","emails":["ciro@example.com"]}';
      print(jsonEncode(userObject));
  final ciroJsonMap = jsonDecode(ciroJsonString);
  print(ciroJsonMap);
  /*
  jsonMap is dynamic by default.
  add 'implicit-dynamic: false' in analysis_option.yaml and you have to declare explicity the dynamic type
  */

  if (ciroJsonMap is Map<String, dynamic>) {
    print('You\'ve got a map!');
  } else {
    print('Your JSON must have been in the wrong format');
  }

  final userCiro = User.fromJson(ciroJsonMap);
  print(userCiro);

}

class User {
  const User({
    required this.id,
    required this.name,
    required this.emails,
  });

  final int id;
  final String name;
  final List<String> emails;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'emails': emails,
    };
  }

  factory User.fromJson(Map<String, dynamic> jsonMap) {
    
    dynamic id = jsonMap['id'];
    if (id is! int) id = 0;

    dynamic name = jsonMap['name'];
    if (name is! String) name = '';

    dynamic maybeEmails = jsonMap['emails'];
    final emails = <String>[];
    if (maybeEmails is List) {
      for (dynamic email in maybeEmails) {
        if (email is String) emails.add(email);
      }
    }

    return User(id: id, 
    name: name,
    emails: emails
    );
  }

@override
String toString() {
  return 'User(id: $id, name: $name, emails: $emails)';
}

}
