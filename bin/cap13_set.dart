import 'dart:collection';
import 'dart:math';

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 13: Nullability
////////////////////////////////////////////////////////////////////////////////////////////////////////

void main() {
  // Initialize
  final Set<int> someSet = {};
  final someSet2 = <int>{};

  // Operation
  someSet.add(12);
  someSet.contains(12);
  someSet.remove(12);
  someSet.addAll([1, 2, 3, 3]);

  for (final value in someSet) {
    print(value);
  }
  ;

  final someNewSet = someSet.toSet();
  someNewSet.remove(2);

  print(someSet);
  print(someNewSet);

  // Set Relationships
  final setA = {8, 2, 3, 1, 4};
  final setB = {1, 6, 5, 4};

  final intersecation = setA.intersection(setB);
  final union = setA.union(setB);
  final diffA = setA.difference(setB);
  final diffB = setB.difference(setA);

  print(intersecation);
  print(union);
  print(diffA);
  print(diffB);

  // Finding duplicates
  final randomGenerator = Random();
  final randomIntList = <int>[];

for (int i = 0; i < 10; i++) {
  final randomInt = randomGenerator.nextInt(10) + 1;
  randomIntList.add(randomInt);
}

print(randomIntList);

final uniqueValues = <int>{}; // declare empty set
final duplicates = <int>{};

randomIntList.forEach((element) {
  if (uniqueValues.contains(element)) { duplicates.add(element); };
uniqueValues.add(element);
});

print('duplicates: $duplicates');
}
