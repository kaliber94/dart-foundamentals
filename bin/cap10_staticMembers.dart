import 'dart:math';

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 10: Static Members
////////////////////////////////////////////////////////////////////////////////////////////////////////

class SomeClass {
  static int myProperty = 0;

  static void myMethod() {
    print('Hello, Dart!');
  }
}

// Costant
class TextStyle {
  static const _defaultFontSize = 17.0;

  TextStyle({this.fontSize = _defaultFontSize});
  final double fontSize;
}

class Colors {
  static const int red = 0xFFD13F13;
  static const int purple = 0xFF8107D9;
  static const int blue = 0xFF1432C9;
}

// Singleton pattern
class MySingleton {
  MySingleton._(); // private named constructor

  /*
  use instance
  static final MySingleton instance = MySingleton._(); 
  */

/* Use factory constructor
*/
  static final MySingleton _instance = MySingleton._();
  factory MySingleton() => _instance;
}

// Static Methods
class Math {
  static num max(num a, num b) {
    return (a > b) ? a : b;
  }

  static num min(num a, num b) {
    return (a < b) ? a : b;
  }
}

// Create new object with static methods
class MyClass {
  int _id;
  String _name;

  MyClass({int id = 0, String name = 'anonymous'})
      : _id = id,
        _name = name {
    print('User created with name $_name');
  }

  // Factory-named Constructor
  MyClass.json(Map<String, Object> json)
      : _id = json['id'] as int,
        _name = json['name'] as String;

  // Factory Constructor
  factory MyClass.fromJsonFunc(Map<String, Object> json) {
    final userId = json['id'] as int;
    final userName = json['name'] as String;
    return MyClass(id: userId, name: userName);
  }

  // Static Constructor
  static MyClass fromJson(Map<String, Object> json) {
    final userId = json['id'] as int;
    final userName = json['name'] as String;
    return MyClass(id: userId, name: userName);
  }

  /*
  A factory constructor can only return an instance of the class type or subtype, whereas a static method can return
  anything. For example, a static method can be asynchronous and return a Future, but a factory constructor can’t do this.
  */

  @override
  String toString() {
    return 'NewUser - Name: $_name, ID: $_id';
  }

  String toJSON() {
    return '{"id":$_id, "name":$_name}';
  }
}

// Challenge
class Sphere {
  final double _radius;

  static const double pi = 3.14;

  const Sphere({required double radius}) : _radius = radius;

  double get area => 4 * pi * pow(_radius, 2);
  double get volume => (pow(_radius, 3) * 4 * pi) / 3;
}

void main() {
  final value = SomeClass.myProperty;
  SomeClass.myMethod();

  final backgroundColor = Colors.purple;

  // use instance
  // final mySingleton = MySingleton.instance;

  // use factory constructor
  final mySingleton = MySingleton();

  print(Math.max(2, 3));
  print(Math.min(2, 3));

  final map = {'id': 99, 'name': 'John'};
  final john = MyClass.fromJson(map);
  print(john);

  final sphere = Sphere(radius: 12);
  print('Area of your sphere is ${sphere.area}');
  print('Volume of your sphere is ${sphere.volume}');
}