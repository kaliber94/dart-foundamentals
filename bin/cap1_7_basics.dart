import 'dart:ffi';
import 'dart:math';
import 'package:characters/characters.dart';

/*
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 1-7: The basics
////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void main() {
  print('Hello, Dart!');
  print(22 ~/ 7);
  print(cos(135 * pi / 180));
  print((1 / sqrt(2)) == sin(pi / 4));

  int number = 10;
  number = 15;

  print(number);

  var valore;
  valore = 10;
  print(valore);

  const myCostant = 10;

  final hoursSinceMidnight = DateTime.now().hour;
  print(hoursSinceMidnight);

  const testNumber = 12;
  const evenOdd = testNumber % 2;
  print(evenOdd);

  int age = 16;
  print(age);

  age = 30;
  print(age);

  num myNum = 3.14;
  print(myNum is double);
  print(myNum is int);
  print(myNum.runtimeType);

  num someNumber = 3;
  final someInt = someNumber as int;
  print(someInt.isEven);

  dynamic myVar = 42;
  myVar = 'pluto';

  const value = 10 / 2;
  print(value.runtimeType);

  print('pippo'.characters.length);
  // print(myVar.characters);

  const name = 'John';
  var intro = 'My name is $name';
  print(intro);

  const oneThird = 1 / 3;
  var sentence = 'One third is ${oneThird.toStringAsPrecision(2)}';
  print(sentence);

  print('I \u2764 Dart\u0021');

  const score = 83;
  int? ciao;
  int? ciao2 = 2;
  const message = (score > 60) ? 'You passed' : 'You failed';
  print(message);

  const weatherDay = Weather.snowy;

  switch (weatherDay) {
    case Weather.sunny:
      print('sunny');
      break;
    case Weather.snowy:
      print('snowy');
      break;
    case Weather.cloudy:
      print('cloudy');
      break;
  }

  const list = [1, 2, 3, 4, 5, 6];

  for (var element in list) {
    print(element.toString());
  }

  list.forEach((element) {
    print(element.toString());
  });

  print(function(54));

  hello('john', 'pluto');

  print(fullname('Ale', '', ''));
  print(sum(10));
  print(sum(10, 20));
  print(secondSum(numero: 10));

  print(getTripled(7));
  print(getTripledInline(10));

  print("Fibonacci of 5 is ${calculateFibonacci(5)}");
  print("Fibonacci of 15 is ${calculateFibonacci(15)}");
  print("Fibonacci of 13 is ${calculateFibonacci(13)}");
  print("Fibonacci of 21 is ${calculateFibonacci(21)}");
}

enum Weather { sunny, snowy, cloudy }

// Func with return value with one parameter
String function(int number) {
  return '$number is a very nice number';
}

// Func with no return value and two parameters
void hello(String person, String pet) {
  print('Hello $person, hi $pet');
}

// Func with optional parameter
String fullname(String firstName, String lastName, [String? title]) {
  return '${title ?? ''} $firstName $lastName';
}

// Func with default value for one parameter
int sum(int value2, [int value = 0]) {
  return value + value2;
}

// Func with explicit parameter name
int secondSum({required int numero, int value = 0}) {
  return value + numero;
}

// Same func written in inline mode:
int getTripled(int number) {
  final tripled = number * 3;
  return tripled;
}

int getTripledInline(int number) => (number * 3);

// Challenge, calculate nth Fibonacci's number
int calculateFibonacci(int nValue) {
  if (nValue == 0 || nValue == 1) {
    return 1;
  }
  int prev = 1;
  int num = 1;
  int index = 1;
  do {
    num += prev;
    prev = num - prev;
    index += 1;
  } while (index < nValue);
  return num;
}