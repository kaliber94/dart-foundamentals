import 'cap8_9_classes.dart';

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 11: Nullability
////////////////////////////////////////////////////////////////////////////////////////////////////////


class NullableUser {
  NullableUser({this.name});
  String? name;
}

class TextWidget {
  String? text;

  bool isLong() {
    final text = this.text;
    if (text == null) {
      return false;
    }
    return text.length < 100;
  }
}

class Event {
  Event(this.name);

  final String name;

  /*
  The instance member '_calculateSecret' can't be accessed in an initializer.
  To solve this problem, you can use the 'late' keyword.
  This is also known as lazy initialization
  */
  late final int _secretNumber = _calculateSecret();

  int _calculateSecret() {
    return name.length + 42;
  }
}

class SomeClass {
  // The method doHeavyCalculation is only run after you access never access it, you never do the work. value the first time.
  late String? value = doHeavyCalculation();
  String? doHeavyCalculation() {
    // do heavy calculation
  }
}

// Challenge: Naming Customs
class Name {
  final String givenName;
  final String? surname;
  final bool surnameIsFirst;

  Name({required this.givenName, this.surname, this.surnameIsFirst = false});

  @override
  String toString() {
    var surname = this.surname;
    if (surname == null) {
      return givenName;
    } else {
      return surnameIsFirst ? (surname + ' ' + givenName) : (givenName + ' ' + surname);
    }
  }
}

void main() {
  int? age;
  String? message;

  print(age);
  print(message);

  final text = message ?? 'Error';

  print(message?.length);
  message = 'hei';
  print(message.length);

  double? fontSize;

  fontSize ??= 20.0; // => fontSize = fontSize ?? 20.0

  String? myNullableString = 'ok';

  String nonNullableString = myNullableString!;

  print(nonNullableString);

  User? user;

  user
    ?..name = 'Ho'
    ..id = 123;

  String? lenghtString = user?.name?.length.toString();
  print(lenghtString);

  final nUser = NullableUser(name: null);

  final event = Event('party');
  print(event._calculateSecret());

  final concettina =
      Name(givenName: 'concettina', surname: 'esposito', surnameIsFirst: true);
  final pino =
      Name(givenName: 'pino', surname: 'di salvo', surnameIsFirst: true);
  final timcook = Name(givenName: 'tim cook', surnameIsFirst: false);

  print(concettina.toString());
  print(pino.toString());
  print(timcook.toString());
}

bool isPositive(int? anInteger) {
  if (anInteger == null) {
    return false;
  }
  return !anInteger.isNegative;
}
