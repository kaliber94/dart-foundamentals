import 'dart:math';

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Chapter 15: Iterables
////////////////////////////////////////////////////////////////////////////////////////////////////////
///

void main() {
  // List are iterables
  final myList = [
    'bread',
    'cheese',
    'milk',
  ];

  print(myList);

  for (final item in myList) {
    print(item);
  }

  final reversedItem = myList.reversed;
  print(reversedItem);

  // Instantiate an iterable

  // final iterable = Iterable(); - error
  Iterable<String> myIterable = ['bread', 'cheese', 'milk'];

  // Access an element
  final thirdElement = myIterable.elementAt(2);
  print(thirdElement);

  final first = myIterable.first;
  final last = myIterable.last;

  // Iterable's methods
  final mapped = myIterable.map((e) => 'Ciao $e');
  print(mapped);

  final filtered = myIterable.where((element) => element.contains('b'));
  print(filtered);

  final expanded = myIterable.expand((element) => [element, element]);
  print(expanded);

  final isContained = myIterable.contains('bread');
  print(isContained);

  myIterable.forEach((element) {
    print('For each $element');
  });

  final reduced = myIterable.reduce((value, element) => value + element);
  print(reduced);

  final folded = myIterable.fold('bread cheese milk hello',
      (previousValue, element) => previousValue.replaceFirst(element, ''));
  print(folded);

  // Creating an iterables from scratch
  final squares = hundredSquares();
  for (int square in squares) {
    print(square);
  }

  // Using an iterator
  final squaressss = HundredSquares();

  for (int squareNumber in squaressss) {
    print(squareNumber);
  }
}

/*
sync => read that as“syncstar”.You’re telling Dart that this function is asynchronous generator. 
        You must return an Iterable from such a function

yield =>  This is similar to the return keyword for normal functions except that yield doesn't exit the function.
          Instead, yield generates a single value and then pauses until the next time you request a value from the iterable.
          Because iterables are lazy, Dart doesn't start the generator function until the first time you request a value from the iterable.
*/
Iterable<int> hundredSquares() sync* {
  for (int i = 1; i <= 100; i++) {
    yield i * i;
  }
}

// Implement an iterator
class SquaredIterator implements Iterator<int> {
  int _index = 0;

  /*
  Every time moveNext is called, you add one to index This incrementally moves index from 1 to 100.
  Once _index reaches 101, moveNext will return false, signaling that the iterator has reached the end of the collection.
  */
  @override
  bool moveNext() {
    _index++;
    return _index <= 100;
  }

  /*
  Sometimes, it's useful to have current return a stored private value, which you could name current,
  for example. However, in this case, it was easy enough to just multiply _index by itself right here to find the square.
  */
  @override
  int get current => _index * _index;
}

class HundredSquares extends Iterable<int> {
  @override
  Iterator<int> get iterator => SquaredIterator();
}
